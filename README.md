# nova-dash

**nova-dash** provides integration with **[Dash](https://kapeli.com/dash)**.

## Requirements

- [Dash](https://kapeli.com/dash)

## Usage

Select the **Editor → Search in Dash** menu item and the word your cursor is in will be searched for in Dash. The appropriate docset will also be selected.
