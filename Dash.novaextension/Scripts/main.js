exports.activate = function() {
    // Do work when the extension is activated
}

exports.deactivate = function() {
    // Clean up state before the extension is deactivated
}

function docsetList(mode, extension) {

  var c = ["c", "glib", "gl2", "gl3", "gl4", "manpages"];
  var css = ['css', 'bootstrap', 'foundation', 'awesome', 'cordova', 'phonegap'];
  var sass = [ 'sass', 'compass','bourbon','neat'].concat(css);
  var js = [ 'javascript', 'jquery', 'jqueryui', 'jquerym', 'react', 'nodejs', 'angularjs', 'backbone', 'marionette', 'meteor', 'moo', 'prototype', 'ember', 'lodash', 'underscore', 'sencha', 'extjs', 'titanium', 'knockout', 'zepto', 'cordova', 'phonegap', 'yui', 'unity3d'];
  var web = [ 'html', 'svg','statamic'].concat(js, css);
  var coffeescript = [ 'coffee'].concat(web);
  var php = [ 'php', 'wordpress', 'drupal', 'zend', 'laravel', 'yii', 'joomla', 'ee', 'codeigniter', 'cakephp', 'phpunit', 'symfony', 'typo3', 'twig', 'smarty', 'phpp', 'mysql', 'sqlite', 'mongodb', 'psql', 'redis', 'html', 'statamic', 'svg', 'css', 'bootstrap', 'foundation', 'awesome'];
  var ruby = [ 'ruby','rubygems','rails' ];
  var objectiveC = [ 'iphoneos', 'macosx', 'appledoc', 'cocos2d', 'cocos3d','kobold2d', 'sparrow','c','manpages'];
  var python = [ 'python3', 'python','django','twisted','sphinx','flask','tornado','sqlalchemy','numpy','scipy','salt','cvp'];
  var cpp = ["cpp", "net", "boost", "qt", "cvcpp", "cocos2dx", "c", "manpages"];
  
  var extensionToMode = {
    'js': 'javascript',
    'rb': 'ruby',
  }

  var modeToKeysMap = {
    "advphp"   : php,
    "apache" : ["apache"],
    "arduino": ["Arduino"],
    "asciiDoc" : [],
    "c"                     : c,
    "csharp"                : ['net'].concat(c),
    "c99"                   : c,
    "c++"                   : cpp,
    "c++11"                 : cpp,
    "coffeescript"          : ["coffee"],
    "css"                   : css,
    "dart"                  : ["dartlang", "polymerdart", "angulardart"],
    "django"                : python.concat(web),
    "erb"                   : ["erb"].concat(web).concat(ruby),
    "go"                    : ["go", "godoc"],
    "haml"                  : ["haml"],
    "html"                  : web,
    "html+django"           : python.concat(web),
    "html+erb"              : ["erb"].concat(web).concat(ruby),
    "html+jinja"            : ["jinja"].concat(python),
    "ini"                   : ["php", "ansible"],
    "javascript"            : js,
    "latex"                 : ["latex"],
    "less"                  : ["less"],
    "lua"                   : ["lua", "corona", "pdx"],
    "markdown"              : ["markdown"],
    "nginx"                 : ["nginx"],
    "nginx+jinja"           : ["nginx", "jinja"],
    "objective-c"           : objectiveC,
    "perl"                  : ["perl", "manpages"],
    "php"                   : php,
    "python"                : python,
    "r"                     : ["r"],
    "ruby"                  : ruby,
    "rust"                  : ["rust"],
    "sass"                  : sass,
    "shell"                 : ["bash", "manpages"],
    "smarty"                : ["smarty"].concat(php),
    "sql"                   : ["mysql", "sqlite", "psql"],
    "swift"                 : ["swift"].concat(objectiveC),
    "typescript"            : ["typescript"].concat(js),
    "xml"                   : ["xml", "titanium"],
    "yaml"                  : ["chef", "ansible", "yaml"]
  };

  if (mode in modeToKeysMap) {
    return modeToKeysMap[mode];
  } else {
    if (extension in extensionToMode) {
      mode = extensionToMode[extension];
      
      if (mode in modeToKeysMap) {
        return modeToKeysMap[mode];
      } else {
        return [];        
      }
    }
  }
}

nova.commands.register("searchInDash", (editor) => {
  var selectedRange = editor.selectedRange;

  if (selectedRange.empty) {
    editor.selectWordsContainingCursors();
    selectedRange = editor.selectedRange;
  }

  if (! selectedRange.empty) {
    var text = editor.getTextInRange(selectedRange);

    var extension = nova.path.extname(editor.document.path);
    // console.log(extension);

    var mode = editor.document.syntax;
    // console.log(mode);

    var docsets = docsetList(mode, extension).join(',');

    var url = "dash-plugin://";

    if (docsets) url += "keys=" + docsets + '&';

    url += "query=" + encodeURIComponent(text);

    // console.log(url);
    nova.openURL(url);

  }
});

